/**
 * Created by hider on 23/03/2019.
 */
window.Vue = require('vue');
import Vuex from 'vuex' ;
import moment from 'moment';
import axios from 'axios';
Vue.use(Vuex);
 export  default    new Vuex.Store({
    state:{
        users : [],
        lastId: null,
        su : {
            auth:   false,
            api_token : '',
            username : '',
            email : null,
        },
       },
    mutations: {
        delete(state,p) {
            // изменяем состояние
            let index  = state.users.findIndex(d => d.id == p)
            state.users.splice(index ,1)
        },

        load(state,payload){
           state.users  = payload
           state.lastId = payload[payload.length-1].id
           
        },

        add_active(state,u){
            state.su.auth  = u
           
        },
        setlastId(state,u){
            
            state.id = u
        },
        set_token(state,u){
            state.su.api_token  = u
        },
        set_email(state,u){
            state.su.email = u
        }
        
    },
    actions :{
        async getUsers({ commit, state }){

            axios.get('/users')
                .then(function (response) {
                    let data = response.data;
                    commit('load',data)
                })
                .catch(function (error) {
                    console.log(error);
                });

        },

        async lastIdGet({commit,state}){
             await axios.get('/last_id').then(res=>{
                 commit('setlastId',res.data.id)

            }).catch(res=>{

            })

        },
       

        async createUser({commit,state,dispatch},u){
           
           let expire = null
            let last  = Number(state.lastId) + 1
           
            switch(u.type){
                case 1: {
                    expire = moment().add(1, 'days').format("MM-DD-YYYY");
                    break;
                }
                case 2: {
                    expire =  moment().add(1, 'months').format("MM-DD-YYYY");
                    break;
                }
                case 3: {
                    expire =  moment().add(6, 'months').format("MM-DD-YYYY");
                    break;
                }
                case 4: {
                    expire = moment().add(365, 'days').format("MM-DD-YYYY");

                    break;
                }
                default : {
                    if(u.choice == true){
                        expire = moment(u.date2,"YYYY-MM-DD")
                        u.created  = u.date
                    }
                    else{
                        expire =  moment().add(1, 'days').format("MM-DD-YYYY");
                    }

                }
            }
            let  reference = 0
            for(let i =1 ; i < 20 ; i++)
            {
                reference += Math.floor(Math.random() * 1000) + 1

            }
            if(u.bundle == '') u.bundle = 'Basic'
            if(u.email == '') u.email = 'sample@account.com'
            if(u.note == '') u.note = 'note'
            if(u.phone == '') u.phone = '07000000000'

            let user = {
                id: last,
                name: u.name,
                email: u.email,
                phone: u.phone,
                status : u.status,
                created : moment().format("YYYY-MM-DD"),
                expire :moment(expire,'MM-DD-YYYY').format("YYYY-MM-DD"),
                bundle: u.bundle,
                note: u.note ,
                reference: 'IT'+reference
            }

            axios.post('/add_user',{
              user

            }).then(function(res){
                state.users.push(user);

            }).catch(function (err) {
               console.log(err)
            })
        },
        async deleteUser({commit,state},idu){

              axios.get('/delete_user/'+idu).then(function (res) {
                  console.log(res);

              }).catch(function(err){
                  console.log(err)
              })
            commit("delete",idu)
        },
        async updateStatus({commit,state},param){
           console.log(param.status)
             axios.post('/update_status',{

                id : param.id,
                status :param.status ,
            }).then(function (res) {


            }).catch(function(err){
                console.log(err)
            })

        },
        async updateNote({commit,state},payload){

            axios.post('/update_note',{
                id : payload.id,
                note : payload.note
            }).then((res)=>{

            }).catch((res)=>{

            })

        },
        async Auth({commit,state},p){
            axios.post('/auth',p).then(res => {
                //console.log(res.data)
              if( res.data.api_token != null){
                  commit("add_active", true)
                  commit("set_token",res.data.api_token)
                  commit("set_email",res.data.email)
                  localStorage.setItem('api_token',res.data.api_token)
                  localStorage.setItem('email',res.data.email)
                  localStorage.setItem('auth','true')
                 }
                 
             }).catch(res => {
               console.log("error "+ res.data)
            });
            
      },
      async passworChange({commit,state},p){
        axios.post('/changepassword',p).then(res => {
            console.log(res.data)
           }).catch(res => {
           console.log("error "+ res.data)
        });
        
  },


        async updateUser({commit,state},u){
            console.log(u.date,u.date2,u.choice)
            let expire = null
            switch(u.type){
                case 1: {
                    u.expire = moment(u.expire).add(1, 'days').format("YYYY-MM-DD");
                     expire  = u.expire
                    break;
                }
                case 2: {
                    u.expire =  moment(u.expire).add(1, 'months').format("YYYY-MM-DD");
                    expire  = u.expire
                    break;
                }
                case 3: {
                    u.expire =  moment(u.expire).add(6, 'months').format("YYYY-MM-DD");
                    expire  = u.expire
                    break;
                }
                case 4: {
                    u.expire = moment(u.expire).add(365, 'days').format("YYYY-MM-DD");
                    expire  = u.expire
                    break;
                }
                default : {
                    if(u.choice == true){
                        u.expire= moment(u.date2,"YYYY-MM-DD").format('YYYY-MM-DD')
                        u.created  = u.date
                        expire = u.expire
                    }
                    else{
                        expire = moment(u.expire,"YYYY-MM-DD").format('YYYY-MM-DD')
                    }


                }

            }
            if(u.phone == '') u.phone = '447000000000'
            let update = {
                id: u.id,
                name: u.name,
                email: u.email,
                phone: u.phone,
                status : u.status,
                created : u.created,
               // expire :moment(expire,'MM-DD-YYYY').format("YYYY-MM-DD"),
                expire: expire,
                bundle: u.bundle,
                note: u.note ,
                reference: u.reference
            }
            axios.post('/update_user',{
                update

            }).then(function(res){


            }).catch(function (err) {
                console.log(err)
            })

        }
    }
});
